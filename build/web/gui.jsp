<%--gui.jsp--%>

<%@page session="false" import="java.util.Iterator"%>

<%--Retrieve the Status bean from scope--%>
<%-- --------------------------------------------- --%>
<jsp:useBean id="status" scope="request" class="util.Status"/>


<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
    <head>
        <title>Currency Converter</title>
    </head>

    <body>

        <h1>Currency Converter</h1>

        <%--Verification error presentation--%>
        <%--
            <%if ((status!=null) && !status.isSuccessful()) 
            {%>
                <font color="red">There were problems processing your request:   
                <ul><%Iterator errors=status.getExceptions();
                while (errors.hasNext()) 
                {
                    Exception ex=(Exception) errors.next();%>
                    <li>
                        <%= ex.getMessage()%>
                    <%}%></ul></font>      
                <%}%>   
            <c:out value="${status.isSuccessful}"></c:out><br>
        --%>

        <c:if test="${!status.isSuccessful}">
            <font color="red">There were problems processing your request:      
                <ul>
                    <c:forEach var="ex" items="${status.exceptions}">
                        <li>  
                        <c:out value="${ex.message}"></c:out>
                    </c:forEach>        
                </ul>
        `   </font>    
        </c:if>
        
        
        <%-- <c:if test="${status.isSuccessful}">
            <jsp:forward page="results.jsp" >
                <jsp:param name="currencyFrom" value="<%=request.getParameter("currencyFrom")%>" />
                <jsp:param name="currencyTo" value="<%=request.getParameter("currencyTo")%>" />
                <jsp:param name="amount" value="<%=request.getParameter("amount")%>" />
            </jsp:forward>
        </c:if>  --%>
      
        <%--Input fields presentation - lists all the fields for entering 
        Web application parameters--%>

        <%--            BEGIN FORM              --%>  
        <%--  <form action="hire" method="post">  --%>
        
        
        <%
            String nextAction = "hire";
            //if (status.isSuccessful()) {
            //    nextAction = "results.jsp";
            //}
        %>
        <%--  <form action="<%=nextAction%>" method="post">  --%>
        <%--  <form name="form1" action="hire" method="post">  --%>
        <form name="form1" action="results.jsp" method="post">
            <hr/>

            <table>
            
                <%--The if statements inside <option �> tags fill in the default values 
                of the combo-boxes from the parameters passed in from last request--%>

                <tr>
                    <td>Please select the currency you want to convert from:</td>
                    <td>
                        <select name="currencyFrom">
                            <%String currencyFrom=request.getParameter("currencyFrom"); if (currencyFrom==null) {currencyFrom="";}%>
                            <option name="currencyFrom" value="European Euro (EUR)" <%if (currencyFrom.equals("European Euro (EUR)")) {out.print(" selected");} %>>European Euro (EUR)</s:option>
                            <option name="currencyFrom" value="British Pound (GBP)" <%if (currencyFrom.equals("British Pound (GBP)")) {out.print(" selected");} %>>British Pound (GBP)</s:option>
                            <option name="currencyFrom" value="Canadian Dollar (CAD)" <%if (currencyFrom.equals("Canadian Dollar (CAD)")) {out.print(" selected");} %>>Canadian Dollar (CAD)</s:option>
                            <option name="currencyFrom" value="Japanese Yen (JPY)" <%if (currencyFrom.equals("Japanese Yen (JPY)")) {out.print(" selected");} %>>Japanese Yen (JPY)</s:option>
                            <option name="currencyFrom" value="Swiss Franc (CHF)" <%if (currencyFrom.equals("Swiss Franc (CHF)")) {out.print(" selected");} %>>Swiss Franc (CHF)<                         <option name="currencyFrom" value="U.S. Dollars (USD)" <%if (currencyFrom.equals("U.S. Dollars (USD)")) {out.print(" selected");} %>>U.S. Dollars (USD)</s:option>
                        </select>
                    </td>
                </tr>


                <tr>
                    <td>Please select the currency you want to convert to:</td>
                    <td>
                        <select name="currencyTo">
                            <%String currencyTo=request.getParameter("currencyTo"); if (currencyTo==null) {currencyTo="";}%>
                            <option name="currencyTo" value="European Euro (EUR)" <%if (currencyTo.equals("European Euro (EUR)")) {out.print(" selected");} %>>European Euro (EUR)</s:option>
                            <option name="currencyTo" value="British Pound (GBP)" <%if (currencyTo.equals("British Pound (GBP)")) {out.print(" selected");} %>>British Pound (GBP)</s:option>
                            <option name="currencyTo" value="Canadian Dollar (CAD)" <%if (currencyTo.equals("Canadian Dollar (CAD)")) {out.print(" selected");} %>>Canadian Dollar (CAD)</s:option>
                            <option name="currencyTo" value="Japanese Yen (JPY)" <%if (currencyTo.equals("Japanese Yen (JPY)")) {out.print(" selected");} %>>Japanese Yen (JPY)</s:option>
                            <option name="currencyTo" value="Swiss Franc (CHF)" <%if (currencyTo.equals("Swiss Franc (CHF)")) {out.print(" selected");} %>>Swiss Franc (CHF)</s:option>
                            <option name="currencyTo" value="U.S. Dollars (USD)" <%if (currencyTo.equals("U.S. Dollars (USD)")) {out.print(" selected");} %>>U.S. Dollars (USD)</s:option>
                        </select>
                    </td>
                </tr>

                <%--The default values of text fields are filled in from the parameters passed in from last request--%>
                <tr>
                    <td>Please enter the amount of currency you want to convert (0 to 1,000,000):</td>
                    <td><%String amount=request.getParameter("amount"); if (amount==null) amount="";%>
                        <input type="text" name="amount" value="<%=amount%>" size="50">
                    </td>
                </tr>
                
            </table>
                    
            <hr/>
            
            <%--  <input type="submit" value="Convert">  --%>
            <input type="submit" value="Convert" onclick="button1()">
        </form>
        
        <SCRIPT LANGUAGE="JavaScript">
            <!--
                function button1()
                {
                    //form1.submit();
                    if (status.isSuccessful()) {
                        //document.forms[].action = 'results.jsp'
                        System.out.println("The button registers!");
                    }
                } 
        // --> 
        </SCRIPT>
    </body>
</html>

