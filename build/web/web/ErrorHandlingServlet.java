//HiringServlet.java 
package web;

//Servlet imports
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import java.io.IOException;

//Support classes
import util.Status;

public class ErrorHandlingServlet extends HttpServlet 
{
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws 
    IOException, ServletException 
    {          
        //Dispatch the request
        processRequest(request, response);
    }  
        
    //The method that performs the Control aspects of the Java application   
    public void processRequest(HttpServletRequest request, HttpServletResponse response) 
    throws IOException, ServletException 
    {   
        System.out.println( "OOOOOOOOOOOOOOOO" );
        //Declare the dispatcher for the View
        RequestDispatcher view=null; 

        //Create the Status object and store it in the request for use by the gui.jsp
        Status status = new Status();
        request.setAttribute( "status", status );
      
        //Retrieve the HTML form parameters
        String currencyFrom    = request.getParameter( "currencyFrom"   );
        String currencyTo      = request.getParameter( "currencyTo"     );
        String amount    = request.getParameter( "amount"  );

        // Verify form fields data; create Exception objects if data are missing
        // These "error messages" are presented to the user in a red bulleted list

        if (currencyFrom.equals(currencyTo)) 
            status.addException(new Exception("Please select different currencies. "));
         
        if ( ( amount ==null ) || (amount.length()==0))
            status.addException( new Exception( "Please enter the amount of currency you want to convert. "));            
              
        // In case of errors, forward the request back to gui.jsp    
        if( !status.isSuccessful() ) 
        {
            view=request.getRequestDispatcher("gui.jsp");              
            view.forward(request, response);
        } 
    }
}