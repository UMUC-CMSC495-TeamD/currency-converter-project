/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package util;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.*;
import org.xml.sax.SAXException;

/**
 *
 * @author vqj01_000
 */
public class CurrencyConverter {
    private double amount;
    private String currencyFrom;
    private String currencyTo;
    private String fileTime;
    private double exchangeRate;
    private double convertedAmount;
    Map<String, Double> conversionRates;// = new HashMap<String, Double>();
    
    
    public CurrencyConverter(String fromAmount, String currencyFromCode,
            String currencyToCode) {
        amount = Double.parseDouble(fromAmount);
        initializeConverter(currencyFromCode, currencyToCode);
    }
    
    //public CurrencyConverter(double fromAmount, String currencyFromCode,
    //        String currencyToCode) {
    //    amount = fromAmount;
    //    initializeConverter(currencyFromCode, currencyToCode);
    //}
    
    private void initializeConverter(String currencyFromCode, String currencyToCode) {
        if (currencyFromCode.length() == 3) {
            currencyFrom = currencyFromCode;
            currencyTo = currencyToCode;
        } else {
            currencyFrom = convertName(currencyFromCode);
            currencyTo = convertName(currencyToCode);
        }
        fileTime = getCurrentTime();
        
        //temporary fileParser to test class
        fileParser();
        
        setExchangeRate();
        setConvertedAmount();
        
    }
    
    public String getFileTime() {
        return fileTime;
    }
    
    public double getExchangeRate() {
        return exchangeRate;
    } 
    
    public double getConvertedAmount() {
        return convertedAmount;
    }
    
    private void fileParser() {
        
        RateParser rateParser = new RateParser();
       
        try {
            conversionRates = rateParser.obtainCurrencyList();
            
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(CurrencyConverter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(CurrencyConverter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CurrencyConverter.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        
        //these are temporary for testing until file parser completed
            //conversionRates.put("EUR", 1.0);
            //conversionRates.put("GBP", 0.8222);
            //conversionRates.put("CAD", 1.5167);
            //conversionRates.put("JPY", 142.03);
            //conversionRates.put("CHF", 1.22);
            //conversionRates.put("USD", 1.3826);
        
    }
    
    private void setExchangeRate() {
        double fromRate;
        double toRate;
            fromRate = conversionRates.get(currencyFrom);
            toRate = conversionRates.get(currencyTo);
            exchangeRate = toRate / fromRate;
    }
    
    private void setConvertedAmount() {
        convertedAmount = amount * exchangeRate;
    }
    
    //adapted from code taken from Intro to Java Programming, Liang, pg. 52
    private String getCurrentTime() {
        String currentTime;
        
        long totalMilliseconds = System.currentTimeMillis();
        long totalSeconds = totalMilliseconds / 1000;
        long currentSecond = totalSeconds % 60;
        long totalMinutes = totalSeconds / 60;
        long currentMinute = totalMinutes % 60;
        long totalHours = totalMinutes / 60;
        long currentHour = totalHours % 24;
        
        currentTime = currentHour + ":" + currentMinute + ":" + currentSecond + " GMT";
        return currentTime;
    }
    
    private String convertName(String currencyName) {
        String convertedName;
        
        int nameLength = currencyName.length();
        convertedName = currencyName.substring(nameLength-4, nameLength-1);
        
        return convertedName;
    }
    
}
