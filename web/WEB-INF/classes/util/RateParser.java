/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package util;
import java.util.*;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.*;
import org.xml.sax.*;
import org.xml.sax.helpers.*;
import java.io.IOException;

//import javax.xml.parsers.DocumentBuilder;
//import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;



/**
 *
 * @author vqj01_000
 */
public class RateParser {
    
    private String urlM = "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";
    
    public RateParser()
        {

        }
 
    public Map<String, Double> obtainCurrencyList() throws ParserConfigurationException,
                                                   SAXException,
                                                   IOException
    {
        SAXParserFactory parserFactory = SAXParserFactory.newInstance();
        SAXParser parser = parserFactory.newSAXParser();
 
        XMLReader xmlReader = parser.getXMLReader();
        CurrencyXMLHandler currHandler = new CurrencyXMLHandler();
        xmlReader.setContentHandler( currHandler);
       
        URL url = new URL( urlM);
        xmlReader.parse(new InputSource( url.openStream()));
 
        return currHandler.getCurrencyList();       
    }
    
   public class CurrencyXMLHandler extends DefaultHandler{
  
   private Map<String, Double> currencyList = new HashMap<String, Double>();
  
   @Override
   public void startElement( String theNamespaceURI,
                             String theLocalName,
                             String theQName,
                             Attributes theAtts) throws SAXException
   {
      if( theLocalName.equals("Cube"))
      {
         if( theAtts != null)
         {
            if( theAtts.getValue( "currency") != null)
            {
               String currency = theAtts.getValue("currency");
               String rate = theAtts.getValue( "rate");
               double dRate = Double.parseDouble(rate);
               currencyList.put(currency, dRate);
            }
            else if( theAtts.getValue( "time") != null)
            {
               String time = theAtts.getValue("time");
            }
         }
      }
   }
  
   Map<String, Double> getCurrencyList()
   {
      return currencyList;
   }
}

    
    
    /*
    public RateParser() 
    {
        try {
            new RateParser().start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void start() throws Exception
    {
        URL url = new URL("http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml");
        URLConnection connection = url.openConnection();

        Document doc = parseXML(connection.getInputStream());
        NodeList currNodes = doc.getElementsByTagName("currency");
        NodeList rateNodes = doc.getElementsByTagName("rate");

        for(int i=0; i<currNodes.getLength();i++)
        {
            System.out.println(currNodes.item(i).getTextContent());
        }
    }

    private Document parseXML(InputStream stream)
    throws Exception
    {
        DocumentBuilderFactory objDocumentBuilderFactory = null;
        DocumentBuilder objDocumentBuilder = null;
        Document doc = null;
        try
        {
            objDocumentBuilderFactory = DocumentBuilderFactory.newInstance();
            objDocumentBuilder = objDocumentBuilderFactory.newDocumentBuilder();

            doc = objDocumentBuilder.parse(stream);
        }
        catch(Exception ex)
        {
            throw ex;
        }       

        return doc;
    }
    */
}
