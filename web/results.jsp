<%-- 
    Document   : results
    Created on : Apr 26, 2014, 10:40:23 PM
    Author     : David
--%>




<%@page import="util.CurrencyConverter"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<%--  <jsp:useBean id="converter" scope="request" class="util.CurrencyConverter"/>  --%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%
  CurrencyConverter converter = new CurrencyConverter(request.getParameter("amount"), 
          request.getParameter("currencyFrom"), request.getParameter("currencyTo"));
        request.setAttribute("convertedAmount", converter.getConvertedAmount() );
        request.setAttribute("exchangeRate", converter.getExchangeRate() );        
        request.setAttribute("fileTime", converter.getFileTime() );  
%>

<!DOCTYPE html>

<html>
    <head>
        <title>Currency Converter Results</title>
    </head>
    <body>
        <h1>Currency Converter Results</h1>
        
        <%out.println (request.getParameter( "amount"));%>
        <%out.println (request.getParameter( "currencyFrom"));%>
        = <%out.println (request.getAttribute( "convertedAmount"));%><%out.println (request.getParameter( "currencyTo"));%>
        <br/>
        exchange rate is
        1 <%out.println (request.getParameter( "currencyFrom"));%> = <%out.println (request.getAttribute( "exchangeRate"));%><%out.println (request.getParameter( "currencyTo"));%>
        
        <br/>
        <br/>
        
        Exchange rate data is extracted from the European Central Bank Foreign Exchange Reference Rates <br/>
        <%out.println (request.getAttribute( "fileTime"));%>
        
        <br/>
        <br/>
        
        <form action="gui.jsp" onsubmit="return confirm_entry()">  
            <input type="submit" value="Click to conduct another conversion">  
        </form>
        
    </body>
</html>
